package com.barry.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.barry.model.Customer;
import com.barry.model.CustomerDisplayDetails;
import com.barry.utils.CustomerValidation;
import com.barry.utils.JsonHelper;
import com.barry.utils.SignupUtilsTest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SignupControllerTest extends SignupUtilsTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Autowired
	JsonHelper jsonHealper;

	@Autowired
	CustomerValidation customerValidation;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void testProducer() throws Exception {
		String uri = "/";
		Customer customer = new Customer("Sachin Ramesh Tendulkar", "5 56 43 23 23");
		String inputJson = super.mapToJson(customer);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		CustomerDisplayDetails response = super.mapFromJson(content, CustomerDisplayDetails.class);
		assertNotNull(response);
	}

	@Test
	public void testInvalidProducer() throws Exception {

		String uri = "/";
		Customer customer = new Customer("Sachin Tendulkar", "5 56 43 23 2");
		String inputJson = super.mapToJson(customer);
		mvc.perform(post(uri, "not found").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testInvalidPhone() throws Exception {

		String uri = "/";
		Customer customer = new Customer("Sachin Tendulkar", "+33 5 56 43 23 22");
		String inputJson = super.mapToJson(customer);
		MvcResult mvcResult = mvc
				.perform(post(uri, "not found").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		CustomerDisplayDetails response = super.mapFromJson(content, CustomerDisplayDetails.class);
		assertNotNull(response);
	}

	@Test
	public void testInvalidPhoneNumber() throws Exception {

		String uri = "/";
		Customer customer = new Customer("Sachin Tendulkar", "05 56 43 23 22");
		String inputJson = super.mapToJson(customer);
		MvcResult mvcResult = mvc
				.perform(post(uri, "not found").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		CustomerDisplayDetails response = super.mapFromJson(content, CustomerDisplayDetails.class);
		assertNotNull(response);
		assertEquals(response.getFirstName(), "Sachin");
		assertEquals(response.getLastName(), "Tendulkar");
		assertEquals(response.getPhone(), "0556432322");

	}

	@Test
	public void testInvalidPhoneNum() throws Exception {

		String uri = "/";
		Customer customer = new Customer("Sachin Tendulkar", " 22");
		String inputJson = super.mapToJson(customer);
		mvc.perform(post(uri, "not found").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testPhoneNumberNull() throws Exception {

		String uri = "/";
		Customer customer = new Customer("Sachin Tendulkar", null);
		String inputJson = super.mapToJson(customer);
		mvc.perform(post(uri, "not found").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void testFistandSurname() throws Exception {

		String uri = "/";
		Customer customer = new Customer("SachinTendulkar", "5 54 84 76 76");
		String inputJson = super.mapToJson(customer);
		mvc.perform(post(uri, "not found").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void testFullNameNull() throws Exception {

		String uri = "/";
		Customer customer = new Customer(null, "0556432322");
		String inputJson = super.mapToJson(customer);
		mvc.perform(post(uri, "not found").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andExpect(status().isBadRequest());
	}

}
