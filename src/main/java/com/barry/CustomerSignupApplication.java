package com.barry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerSignupApplication {
	public static void main(String[] args) {

		SpringApplication.run(CustomerSignupApplication.class, args);

	}
}
