package com.barry.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.barry.constants.SignupConstants;
import com.barry.model.Customer;
import com.barry.model.CustomerDisplayDetails;

@Component
public class CustomerValidation {

	private static final Logger LOGGER = LogManager.getLogger(CustomerValidation.class);

	@Autowired
	private CustomerDisplayDetails displayDetails;

	@Autowired
	private JsonHelper jsonHealper;

	@Autowired
	private SignupConstants signupConstants;

	public String getdisplayDetails(Customer customer) {
		String formattedDetails = signupConstants.getInvalid();
		try {
			String fullName = customer.getFullName();
			String phone = customer.getPhone();
			phone = phone != null ? phoneNumber(phone) : signupConstants.getInvalid();
			if (!phone.equalsIgnoreCase(formattedDetails) && null != fullName) {
				String[] NamesSeperated = fullName.split(" ");
				String firstName = NamesSeperated[0];
				String lastName = NamesSeperated[NamesSeperated.length - 1];
				if (!firstName.equalsIgnoreCase(lastName)) {
					displayDetails.setFirstName(firstName);
					displayDetails.setLastName(lastName);
					displayDetails.setPhone(phone);
					formattedDetails = jsonHealper.toJson(displayDetails);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(e);
		}
		return formattedDetails;
	}

	public String phoneNumber(String phone) {
		String phoneNumber = phone.replaceAll(" ", "");
		int phoneNumberLength = phoneNumber.length();
		if (phoneNumberLength > 3) {
			String countryCode = phoneNumber.substring(0, 3);
			String zero = phoneNumber.substring(0, 1);
			if (phoneNumberLength == 12 && (countryCode).equalsIgnoreCase("+33")) {
				return phoneNumber;
			} else if (phoneNumberLength == 10 && (zero).equalsIgnoreCase("0")) {
				return phoneNumber;
			} else if (phoneNumberLength == 9) {
				phoneNumber = ("+33").concat(phoneNumber);
			} else {
				phoneNumber = signupConstants.getInvalid();
			}
		} else {
			return signupConstants.getInvalid();
		}
		return phoneNumber;
	}

}
