package com.barry.utils;

import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonHelper {

	private Gson gson = null;
	private GsonBuilder gsonBuilder = null;
	private String dateFormat = null;

	public JsonHelper(GsonBuilder gsonBuilder, String dateFormat) {
		super();
		// TODO Auto-generated constructor stub
		this.gsonBuilder = gsonBuilder;
		this.dateFormat = dateFormat;
		gsonBuilder.setDateFormat(dateFormat);
		gson = gsonBuilder.create();
	}

	public JsonHelper() {

	}

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}

	public <T> T fromJson(Reader json, Class<T> classOfT) {
		return (this.gson.fromJson(json, classOfT));
	}

	public <T> T fromJson(Reader json, Type typeOfT) {
		return (this.gson.fromJson(json, typeOfT));
	}

	public <T> T fromJson(String json, Class<T> classOfT) {
		return (this.gson.fromJson(json, classOfT));
	}

	public <T> T fromJson(String json, Type typeOfT) {
		return (this.gson.fromJson(json, typeOfT));
	}

	public String toJson(Object src) {
		return (this.gson.toJson(src));
	}

	public String toJson(Object src, Type typeofSrc) {
		return (this.gson.toJson(src, typeofSrc));
	}

	public void toJson(Object src, Type typeofSrc, Writer writer) {
		this.gson.toJson(src, typeofSrc, writer);
	}

	public void toJson(Object src, Writer writer) {
		this.gson.toJson(src, writer);
	}

}
