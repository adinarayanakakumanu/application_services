package com.barry.utils;

public class SignupBusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2967388645094229412L;

	public SignupBusinessException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SignupBusinessException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SignupBusinessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SignupBusinessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SignupBusinessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
