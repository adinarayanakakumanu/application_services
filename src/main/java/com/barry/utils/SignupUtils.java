package com.barry.utils;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;

import com.google.gson.GsonBuilder;
import com.barry.constants.SignupConstants;
import com.barry.service.SignupConsumerService;

@Configuration
public class SignupUtils {

	@Autowired
	private SignupConstants signupConstants;

	@Bean
	Queue queue() {
		return new Queue(signupConstants.getQueueName(), false);
	}

	@Bean
	TopicExchange exchange() {
		return new TopicExchange(signupConstants.getExchangeName());
	}

	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(signupConstants.getRoutingKey());
	}

	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
			MessageListenerAdapter listenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(signupConstants.getQueueName());
		container.setMessageListener(listenerAdapter);
		return container;
	}

	@Bean
	public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
		return new MappingJackson2MessageConverter();
	}

	@Bean
	public RabbitTemplate amqpTemplate(ConnectionFactory connectionFactory) {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(messageConverter());
		return rabbitTemplate;
	}

	@Bean
	public Jackson2JsonMessageConverter messageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	MessageListenerAdapter listenerAdapter(SignupConsumerService listener) {
		return new MessageListenerAdapter(listener, "listen");
	}

	@Bean
	public GsonBuilder jsonBuilder() {
		return new GsonBuilder();
	}

	@Bean
	JsonHelper jsonHelper(GsonBuilder jsonBuilder) {

		return new JsonHelper(jsonBuilder, "dd/MM/yyyy");
	}

}
