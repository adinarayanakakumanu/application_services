package com.barry.controllers;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.barry.constants.SignupConstants;
import com.barry.model.Customer;
import com.barry.model.CustomerDisplayDetails;
import com.barry.service.SignupProducerService;
import com.barry.utils.CustomerValidation;
import com.barry.utils.JsonHelper;

@RestController
@RequestMapping(value = "/")
public class SignupController {

	private static final Logger LOGGER = LogManager.getLogger(SignupController.class);

	@Autowired
	SignupProducerService rabbitMQSender;

	@Autowired
	JsonHelper jsonHelper;

	@Autowired
	private CustomerValidation customerValidation;

	@Autowired
	private SignupConstants signupConstants;

	@Autowired
	CustomerDisplayDetails displayDetails;

	@PostMapping(value = "")
	public CustomerDisplayDetails producer(@RequestBody Customer customer, HttpServletResponse response) {

		String customerDetails = null;
		try {
			LOGGER.info("Post Request received..... ");
			customerDetails = customerValidation.getdisplayDetails(customer);
			if (!(signupConstants.getInvalid()).equalsIgnoreCase(customerDetails)) {
				rabbitMQSender.sendMessage(customer);
				displayDetails = jsonHelper.fromJson(customerDetails, CustomerDisplayDetails.class);
			} else {
				customerDetails = signupConstants.getInvalidJson();
				LOGGER.info("invalid Json..... ");
				response.sendError(HttpStatus.SC_BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error(e.getStackTrace());
		}

		return displayDetails;
	}

}
