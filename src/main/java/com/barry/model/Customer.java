package com.barry.model;

import org.springframework.stereotype.Component;

@Component
public class Customer {

	private String fullName;
	private String phone;

	public Customer() {
		super();
	}

	public Customer(String fullName, String phone) {
		super();
		this.fullName = fullName;
		this.phone = phone;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
