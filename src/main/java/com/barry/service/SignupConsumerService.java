package com.barry.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barry.model.Customer;
import com.barry.model.CustomerDisplayDetails;
import com.barry.utils.CustomerValidation;
import com.barry.utils.JsonHelper;
import com.barry.utils.SignupBusinessException;

@Service
public class SignupConsumerService {

	private static final Logger LOGGER = LogManager.getLogger(SignupConsumerService.class);

	@Autowired
	private JsonHelper jsonHelper;

	@Autowired
	private Customer customer;

	@Autowired
	private CustomerValidation customerValidation;

	@Autowired
	CustomerDisplayDetails displayDetails;

	public void listen(byte[] message) throws SignupBusinessException {
		String msg = new String(message);
		customer = jsonHelper.fromJson(msg, Customer.class);
		String formattedDetails = customerValidation.getdisplayDetails(customer);
		displayDetails = jsonHelper.fromJson(formattedDetails, CustomerDisplayDetails.class);
		LOGGER.info(" Formatted Customer Details Json : " + formattedDetails);
		LOGGER.info(" Consumer Received a new Massage......");
		LOGGER.info(" User " + displayDetails.getFirstName() + " " + displayDetails.getLastName() + " with phone "
				+ displayDetails.getPhone() + " has just signed up!");

	}
}
