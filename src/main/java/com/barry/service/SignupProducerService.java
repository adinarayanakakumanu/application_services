package com.barry.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barry.constants.SignupConstants;
import com.barry.model.Customer;
import com.barry.utils.JsonHelper;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class SignupProducerService {

	private static final Logger LOGGER = LogManager.getLogger(SignupProducerService.class);

	@Autowired
	private AmqpTemplate rabbitTemplate;

	@Autowired
	private SignupConstants signupMQProperties;

	@Autowired
	private JsonHelper jsonHealper;

	public void sendMessage(Customer customer) throws JsonProcessingException {
		String customerDetails = null;
		rabbitTemplate.convertAndSend(signupMQProperties.getExchangeName(), signupMQProperties.getRoutingKey(),
				customer);
		customerDetails = jsonHealper.toJson(customer, Customer.class);
		LOGGER.info("Producer Json: " + customerDetails);
	}

}
